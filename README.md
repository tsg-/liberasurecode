liberasurecode is an Erasure Code API library written in C with pluggable Erasure Code backends.  

The project has a new home under the Openstack namespace - [openstack/liberasurecode][https://github.com/openstack/liberasurecode].  Please update your references.